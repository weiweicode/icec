package org.icec.mybatis.domain;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.icec.common.annotation.Excel;


/**
 * 短信记录对象 sms_record
 * 
 * @author jinxx
 * @date 2020-09-14
 */
public class SmsRecord  
{
    private static final long serialVersionUID = 1L;
    public static final String STATE_NO = "0";
    public static final String STATE_YES = "1";
    public static final String STATE_ERR = "2";

    public static final String TYPE_NOTICE = "1"; // 通知类型
    public static final String TYPE_VERIFY = "2"; // 验证码类型
    
    public static final String DEL_FLAG_NORMAL="0";
    public static final String DEL_FLAG_DELETE="1";
    /** null */
    private Long id;

    /** 如登录，催单，提醒 */
    @Excel(name = "如登录，催单，提醒")
    private String templateName;

    /** 手机号码 */
    @Excel(name = "手机号码")
    private String phone;

    /** 短信模板编码 */
    @Excel(name = "短信模板编码")
    private String templateCode;

    /** 短信模板参数json格式 */
    @Excel(name = "短信模板参数json格式")
    private String param;

    /** 0未发送，1已发送 */
    @Excel(name = "0未发送，1已发送")
    private String state;

    /** null */
    @Excel(name = "null")
    private Long sendby;

    /** null */
    @Excel(name = "null", width = 30, dateFormat = "yyyy-MM-dd")
    private Date sendtime;

    /** null */
    @Excel(name = "null")
    private String note;

    /** null */
    private String delFlag;

    /** null */
    @Excel(name = "null")
    private String code;

    /** 短信类型 */
    @Excel(name = "短信类型")
    private String type;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setTemplateName(String templateName) 
    {
        this.templateName = templateName;
    }

    public String getTemplateName() 
    {
        return templateName;
    }
    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }
    public void setTemplateCode(String templateCode) 
    {
        this.templateCode = templateCode;
    }

    public String getTemplateCode() 
    {
        return templateCode;
    }
    public void setParam(String param) 
    {
        this.param = param;
    }

    public String getParam() 
    {
        return param;
    }
    public void setState(String state) 
    {
        this.state = state;
    }

    public String getState() 
    {
        return state;
    }
    public void setSendby(Long sendby) 
    {
        this.sendby = sendby;
    }

    public Long getSendby() 
    {
        return sendby;
    }
    public void setSendtime(Date sendtime) 
    {
        this.sendtime = sendtime;
    }

    public Date getSendtime() 
    {
        return sendtime;
    }
    public void setNote(String note) 
    {
        this.note = note;
    }

    public String getNote() 
    {
        return note;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("templateName", getTemplateName())
            .append("phone", getPhone())
            .append("templateCode", getTemplateCode())
            .append("param", getParam())
            .append("state", getState())
            .append("sendby", getSendby())
            .append("sendtime", getSendtime())
            .append("note", getNote())
            .append("delFlag", getDelFlag())
            .append("code", getCode())
            .append("type", getType())
            .toString();
    }
}
