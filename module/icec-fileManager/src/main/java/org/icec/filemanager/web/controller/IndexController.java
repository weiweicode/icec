package org.icec.filemanager.web.controller;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("filemanager")
public class IndexController {
    @GetMapping("/index")
    @RequiresPermissions("core:filemanager")
    public String index(){
        return "filemanager/index";
    }
}
