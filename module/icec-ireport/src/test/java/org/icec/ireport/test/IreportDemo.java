package org.icec.ireport.test;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;

public class IreportDemo {
	public static void main(String[] args) throws Exception {
		// 设置模板数据
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("a", "好");
		
		// Field变量
		List<User> list = new ArrayList<User>();
		list.add(new User("1", "ddd"));
		list.add(new User("2", "非常好"));
		
		 String jrxml = "/A_A4.jrxml";
		InputStream input = IreportDemo.class.getResourceAsStream(jrxml);
		JasperReport report = JasperCompileManager.compileReport(input);
		 
		
		JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters,
				new JRBeanCollectionDataSource(list));
		// 生成PDF
		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream("target/out.pdf"));
	}
}
