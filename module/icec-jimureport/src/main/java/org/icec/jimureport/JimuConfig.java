package org.icec.jimureport;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "org.jeecg.modules.jmreport")
@MapperScan(basePackages ={"org.jeecg.modules.**.mapper*"})
public class JimuConfig {

}
