package org.icec.sms.job;

import org.icec.sms.service.SmsService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.stereotype.Component;
/**
 * 短信发送job
 * @author xxjin
 *
 */
@Component
public class SmsJob implements Job{
	SmsService smsService;
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		smsService.sendSmsToAliyun();
	}

}
