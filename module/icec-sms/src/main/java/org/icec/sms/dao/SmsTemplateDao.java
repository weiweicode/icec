package org.icec.sms.dao;
import org.beetl.sql.core.db.KeyHolder;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;
import org.icec.sms.model.SmsTemplate;


/* 
* 
* gen by icec mapper 2018-12-12
*/
public interface SmsTemplateDao extends BaseMapper<SmsTemplate> {
	/*
	*
	*分页查询
	*
	*/
	public PageQuery<SmsTemplate> pageQuery(PageQuery<SmsTemplate> query);
}
