package org.icec.sms.model;
import java.io.Serializable;
import java.math.*;
import java.util.Date;
import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.icec.common.model.BaseModel;

/*
* 
* gen by icec 2018-10-29
*/
public class SmsRecord   implements BaseModel{
	public static final String STATE_NO="0";
	public static final String STATE_YES="1";
	private Integer id ;
	private Integer sendby ;
	private String delFlag ;
	private String note ;
	//短信模板参数json格式
	private String param ;
	//多个号码用，分隔
	private String phone ;
	//0未发送，1已发送
	private String state ;
	//短信模板编码
	private String templateCode ;
	//如登录，催款，提醒
	private String templateName ;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date createtime ;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date sendtime ;
	//平台返回编码
	private String code;
	public SmsRecord() {
	}
	
	public SmsRecord(Integer sendby, String delFlag, String param, String phone, String state, String templateCode,
			Date createtime) {
		super();
		this.sendby = sendby;
		this.delFlag = delFlag;
		this.param = param;
		this.phone = phone;
		this.state = state;
		this.templateCode = templateCode;
		this.createtime = createtime;
	}

	public Integer getId(){
		return  id;
	}
	public void setId(Integer id ){
		this.id = id;
	}
	
	public Integer getSendby(){
		return  sendby;
	}
	public void setSendby(Integer sendby ){
		this.sendby = sendby;
	}
	
	public String getDelFlag(){
		return  delFlag;
	}
	public void setDelFlag(String delFlag ){
		this.delFlag = delFlag;
	}
	
	public String getNote(){
		return  note;
	}
	public void setNote(String note ){
		this.note = note;
	}
	
	public String getParam(){
		return  param;
	}
	public void setParam(String param ){
		this.param = param;
	}
	
	public String getPhone(){
		return  phone;
	}
	public void setPhone(String phone ){
		this.phone = phone;
	}
	
	public String getState(){
		return  state;
	}
	public void setState(String state ){
		this.state = state;
	}
	
	public String getTemplateCode(){
		return  templateCode;
	}
	public void setTemplateCode(String templateCode ){
		this.templateCode = templateCode;
	}
	
	public String getTemplateName(){
		return  templateName;
	}
	public void setTemplateName(String templateName ){
		this.templateName = templateName;
	}
	
	public Date getCreatetime(){
		return  createtime;
	}
	public void setCreatetime(Date createtime ){
		this.createtime = createtime;
	}
	
	public Date getSendtime(){
		return  sendtime;
	}
	public void setSendtime(Date sendtime ){
		this.sendtime = sendtime;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	
	

}
