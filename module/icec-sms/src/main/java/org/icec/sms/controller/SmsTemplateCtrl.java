package org.icec.sms.controller;

import org.beetl.sql.core.engine.PageQuery;
import org.icec.common.base.tips.Tip;
import org.icec.common.web.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.icec.sms.model.SmsTemplate;
import org.icec.sms.service.SmsTemplateService;
import org.icec.web.core.shiro.annotation.CurrentUser;
import org.icec.web.core.sys.model.SysUser;

/*
* 
* gen by icec  2018-12-12
*/
@Controller
@RequestMapping("sms/smsTemplate")
public class SmsTemplateCtrl   extends BaseController{
	@Autowired
	private SmsTemplateService  smsTemplateService ;
	
	@RequestMapping("add")
	public String add(Model model) {
		return "sms/templateAdd";
	}
	/**
	*
	*保存
	*/
	@RequestMapping("save")
	@ResponseBody
	public Tip save(SmsTemplate smsTemplate,   @CurrentUser SysUser optuser ){
		smsTemplateService.save(smsTemplate, optuser);
		return SUCC;
	}
	/**
	 * 进入查询界面
	 * 
	 * @return
	 */
	@RequestMapping("list")
	public String listInit() {
		return "sms/templateList";
	}
	
	/**
	 * 进入修改界面
	 * 
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping("edit/{id}")
	public String edit(@PathVariable Integer id, ModelMap model) {
		SmsTemplate smsTemplate=smsTemplateService.get(id);
		model.addAttribute("smsTemplate", smsTemplate);
		return "sms/templateEdit";
	}
	/**
	 * 更新数据逻辑
	 * 
	 * @param SmsTemplate
	 * @return
	 */
	@RequestMapping("update")
	@ResponseBody
	public Tip update(SmsTemplate smsTemplate ,@CurrentUser SysUser optuser ) {
		smsTemplateService.update(smsTemplate,optuser);
		return SUCC;
	}
	/**
	 * 按主键删除
	 * 
	 * @param ids
	 *@return
	 */
	@RequestMapping("deleteAll")
	@ResponseBody
	public Tip deleteAll(String ids, @CurrentUser SysUser optuser) {
		if (ids == null) {
			return SUCC;
		}
		smsTemplateService.deleteAll(ids, optuser);
		return SUCC;
	}
	/**
	 * 查询逻辑
	 * 
	 * @param pageNumber
	 * @param pageSize
	 * @param user
	 * @return
	 */
	@RequestMapping("query")
	@ResponseBody
	public PageQuery<SmsTemplate> query(@RequestParam(defaultValue = "1") Integer pageNumber, Integer pageSize,
			SmsTemplate smsTemplate) {
		PageQuery<SmsTemplate> query = new PageQuery<SmsTemplate>();
		query.setPageNumber(pageNumber);
		if(pageSize!=null) {
			query.setPageSize(pageSize);
		}
		smsTemplate.setDelFlag(SmsTemplate.DEL_FLAG_NORMAL);
		query.setParas(smsTemplate);
		query = smsTemplateService.pageQuery(query);
		return query;
	}
}
