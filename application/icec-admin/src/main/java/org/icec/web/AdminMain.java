package org.icec.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.boot.Banner;

@SpringBootApplication
@ComponentScan(basePackages= {"org.icec"})
public class AdminMain extends SpringBootServletInitializer {
	/**
	 * 为了支持war发布
	 */
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(AdminMain.class);
	}

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(AdminMain.class);
		app.setBannerMode(Banner.Mode.OFF);
		app.run(args);

	}

	
}
