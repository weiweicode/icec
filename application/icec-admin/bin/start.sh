#!/bin/bash
APP_NAME=ICECAdminMain
MAIN_CLASS=org.icec.web.AdminMain
rm -f tpid
BIN_DIR=$(cd `dirname $0`;pwd)
DEPLOY_DIR=$(cd $BIN_DIR;cd ..;pwd)
LOG_DIR=$DEPLOY_DIR/
JVM_OPTS="-server -Xms2g -Xmx2g"
JVM_PARAS="-DappName=${APP_NAME} -DlogDir=${LOG_DIR} -Dspring.profiles.active=test"
nohup java ${JVM_OPTS} $JVM_PARAS -cp "${DEPLOY_DIR}/lib/*:${DEPLOY_DIR}/config/:${DEPLOY_DIR}/config/logback/" $MAIN_CLASS > /dev/null 2>&1 &

echo $! > tpid

echo Start Success!
