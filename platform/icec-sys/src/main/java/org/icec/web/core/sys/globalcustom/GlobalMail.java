package org.icec.web.core.sys.globalcustom;

import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;

public class GlobalMail implements GlobalConfigurable {
	public static final String PREFIX = "sys_mail";
	public static final String MAIL_SMTP_HOST =  "smtpHost";
	public static final String MAIL_SMTP_PORT =   "smtpPort";
	public static final String MAIL_SMTP_AUTH =   "smtpAuth";
	public static final String MAIL_SMTP_SSL =   "smtpSsl";
	public static final String MAIL_SMTP_TIMEOUT =   "smtpTimeout";
	public static final String MAIL_SMTP_USERNAME =   "smtpUsername";
	public static final String MAIL_SMTP_PASSWORD =   "smtpPassword";
	public static final String MAIL_FROM =   "from";
	public static final String MAIL_TEST_TO =   "testTo";
	public static final String MAIL_TEST_SUBJTCT =   "testSubject";
	public static final String MAIL_TEST_TEXT =   "testText";

	private Map<String, String> customs;

	public GlobalMail() {
	}

	public GlobalMail(Map<String, String> customs) {
		this.customs = customs;
	}


	public String getSmtpHost() {
		return getCustoms().get(MAIL_SMTP_HOST);
	}

	public void setSmtpHost(String smtpHost) {
		if (StringUtils.isNotBlank(smtpHost)) {
			getCustoms().put(MAIL_SMTP_HOST, smtpHost);
		} else {
			getCustoms().remove(MAIL_SMTP_HOST);
		}
	}

	public Integer getSmtpPort() {
		String smtpPort = getCustoms().get(MAIL_SMTP_PORT);
		if (StringUtils.isNotBlank(smtpPort)) {
			return Integer.parseInt(smtpPort);
		} else {
			return null;
		}
	}

	public void setSmtpPort(Integer smtpPort) {
		if (smtpPort != null) {
			getCustoms().put(MAIL_SMTP_PORT, smtpPort.toString());
		} else {
			getCustoms().remove(MAIL_SMTP_PORT);
		}
	}

	public Boolean getSmtpAuth() {
		String smtpAuth = getCustoms().get(MAIL_SMTP_AUTH);
		if (StringUtils.isNotBlank(smtpAuth)) {
			return Boolean.parseBoolean(smtpAuth);
		} else {
			return null;
		}
	}

	public void setSmtpAuth(Boolean smtpAuth) {
		if (smtpAuth != null) {
			getCustoms().put(MAIL_SMTP_AUTH, smtpAuth.toString());
		} else {
			getCustoms().remove(MAIL_SMTP_AUTH);
		}
	}

	public Boolean getSmtpSsl() {
		String smtpSsl = getCustoms().get(MAIL_SMTP_SSL);
		if (StringUtils.isNotBlank(smtpSsl)) {
			return Boolean.parseBoolean(smtpSsl);
		} else {
			return null;
		}
	}

	public void setSmtpSsl(Boolean smtpSsl) {
		if (smtpSsl != null) {
			getCustoms().put(MAIL_SMTP_SSL, smtpSsl.toString());
		} else {
			getCustoms().remove(MAIL_SMTP_SSL);
		}
	}

	public Integer getSmtpTimeout() {
		String smtpTimeout = getCustoms().get(MAIL_SMTP_TIMEOUT);
		if (StringUtils.isNotBlank(smtpTimeout)) {
			return Integer.decode(smtpTimeout);
		} else {
			return null;
		}
	}

	public void setSmtpTimeout(Integer smtpTimeout) {
		if (smtpTimeout != null) {
			getCustoms().put(MAIL_SMTP_TIMEOUT, smtpTimeout.toString());
		} else {
			getCustoms().remove(MAIL_SMTP_TIMEOUT);
		}
	}

	public String getFrom() {
		return getCustoms().get(MAIL_FROM);
	}

	public void setFrom(String from) {
		if (StringUtils.isNotBlank(from)) {
			getCustoms().put(MAIL_FROM, from);
		} else {
			getCustoms().remove(MAIL_FROM);
		}
	}

	public String getSmtpUsername() {
		return getCustoms().get(MAIL_SMTP_USERNAME);
	}

	public void setSmtpUsername(String username) {
		if (StringUtils.isNotBlank(username)) {
			getCustoms().put(MAIL_SMTP_USERNAME, username);
		} else {
			getCustoms().remove(MAIL_SMTP_USERNAME);
		}
	}

	public String getSmtpPassword() {
		return getCustoms().get(MAIL_SMTP_PASSWORD);
	}

	public void setSmtpPassword(String password) {
		if (StringUtils.isNotBlank(password)) {
			getCustoms().put(MAIL_SMTP_PASSWORD, password);
		} else {
			getCustoms().remove(MAIL_SMTP_PASSWORD);
		}
	}

	public String getTestTo() {
		return getCustoms().get(MAIL_TEST_TO);
	}

	public void setTestTo(String testTo) {
		if (StringUtils.isNotBlank(testTo)) {
			getCustoms().put(MAIL_TEST_TO, testTo);
		} else {
			getCustoms().remove(MAIL_TEST_TO);
		}
	}

	public String getTestSubject() {
		return getCustoms().get(MAIL_TEST_SUBJTCT);
	}

	public void setTestSubject(String testSubject) {
		if (StringUtils.isNotBlank(testSubject)) {
			getCustoms().put(MAIL_TEST_SUBJTCT, testSubject);
		} else {
			getCustoms().remove(MAIL_TEST_SUBJTCT);
		}
	}

	public String getTestText() {
		return getCustoms().get(MAIL_TEST_TEXT);
	}

	public void setTestText(String testText) {
		if (StringUtils.isNotBlank(testText)) {
			getCustoms().put(MAIL_TEST_TEXT, testText);
		} else {
			getCustoms().remove(MAIL_TEST_TEXT);
		}
	}

	public Map<String, String> getCustoms() {
		if (customs == null) {
			customs = new HashMap<String, String>();
		}
		return customs;
	}

	public void setCustoms(Map<String, String> customs) {
		this.customs = customs;
	}

	public String getPrefix() {
		return PREFIX;
	}
}
