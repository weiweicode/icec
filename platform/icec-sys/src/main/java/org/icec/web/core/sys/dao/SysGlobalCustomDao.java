package org.icec.web.core.sys.dao;
import org.beetl.sql.core.db.KeyHolder;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;
import org.icec.web.core.sys.model.SysGlobalCustom;


/* 
* 
* gen by icec mapper 2018-12-25
*/
public interface SysGlobalCustomDao extends BaseMapper<SysGlobalCustom> {
	/*
	*
	*分页查询
	*
	*/
	public PageQuery<SysGlobalCustom> pageQuery(PageQuery<SysGlobalCustom> query);
}
