package org.icec.web.core.sys.service;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.beetl.sql.core.engine.PageQuery;
import org.icec.web.core.sys.model.SysUser;
import org.icec.web.core.sys.model.SysGlobalCustom;
import org.icec.web.core.sys.dao.SysGlobalCustomDao;
import org.icec.web.core.sys.globalcustom.GlobalConfigurable;

/*
* 
* gen by icec  2018-12-25
*/
@Service
public class SysGlobalCustomService   {
	@Autowired
	private SysGlobalCustomDao  sysGlobalCustomDao ;
	
	/**
	*
	*保存
	*/
	@Transactional
	public void save(SysGlobalCustom sysGlobalCustom,SysUser optuser){
		sysGlobalCustomDao.insert(sysGlobalCustom);
	}
	/**
	*
	*更新
	*/
	@Transactional
	public void update(SysGlobalCustom sysGlobalCustom,SysUser optuser){
		 
		sysGlobalCustomDao.updateTemplateById(sysGlobalCustom);
	}
	/**
	 * 删除操作，更新del_flag字段
	 * 
	 * @param ids
	 * @param optuser
	 */
	@Transactional
	public void deleteAll(String ids,SysUser optuser) {
		String[] idarr = ids.split(",");
		for (String id : idarr) {
			sysGlobalCustomDao.deleteById(Integer.parseInt(id));
		}

	}
	/**
	*
	*按主键查询
	*
	*/
	public SysGlobalCustom get(Integer id){
		return sysGlobalCustomDao.single(id);
	}
	/**
	*
	*分页查询
	*
	*/
	public PageQuery<SysGlobalCustom> pageQuery(PageQuery<SysGlobalCustom> query){
		return sysGlobalCustomDao.pageQuery(query);
	}
	
	/**
	 * 根据前缀查询
	 * @param <T>
	 * @param prefix
	 * @return
	 */
	public  <T extends GlobalConfigurable> T findByPrefix(T  globalCustom){
		if(globalCustom.getPrefix()==null){return globalCustom;}
		List<SysGlobalCustom> list = sysGlobalCustomDao.createLambdaQuery().andEq(SysGlobalCustom::getfPrefix, globalCustom.getPrefix()).select();
		Map<String, String> customs= new HashMap<String, String>(); 
		for(SysGlobalCustom e:list) {
			customs.put(e.getfKey(), e.getfValue());
		}
		globalCustom.setCustoms(customs);
		return globalCustom;
	}
}
