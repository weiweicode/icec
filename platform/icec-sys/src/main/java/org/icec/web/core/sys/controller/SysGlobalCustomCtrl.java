package org.icec.web.core.sys.controller;

import org.beetl.sql.core.engine.PageQuery;
import org.icec.common.base.tips.Tip;
import org.icec.common.web.BaseController;
import org.icec.web.core.shiro.annotation.CurrentUser;
import org.icec.web.core.sys.model.SysUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.icec.web.core.sys.globalcustom.GlobalMail;
import org.icec.web.core.sys.model.SysGlobalCustom;
import org.icec.web.core.sys.service.SysGlobalCustomService;

/*
* 
* gen by icec  2018-12-25
*/
@Controller
@RequestMapping("sysGlobalCustom")
public class SysGlobalCustomCtrl   extends BaseController{
	@Autowired
	private SysGlobalCustomService  sysGlobalCustomService ;
	
	@RequestMapping("add")
	public String add(Model model) {
		return "sysGlobalCustom/add";
	}
	/**
	*
	*保存
	*/
	@RequestMapping("save")
	@ResponseBody
	public Tip save(SysGlobalCustom sysGlobalCustom,   @CurrentUser SysUser optuser ){
		sysGlobalCustomService.save(sysGlobalCustom, optuser);
		return SUCC;
	}
	/**
	 * 进入查询界面
	 * 
	 * @return
	 */
	@RequestMapping("list")
	public String listInit() {
		return "sysGlobalCustom/list";
	}
	
	/**
	 * 进入修改界面
	 * 
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping("edit/{id}")
	public String edit(@PathVariable Integer id, ModelMap model) {
		SysGlobalCustom sysGlobalCustom=sysGlobalCustomService.get(id);
		model.addAttribute("sysGlobalCustom", sysGlobalCustom);
		return "sysGlobalCustom/edit";
	}
	/**
	 * 更新数据逻辑
	 * 
	 * @param SysGlobalCustom
	 * @return
	 */
	@RequestMapping("update")
	@ResponseBody
	public Tip update(SysGlobalCustom sysGlobalCustom ,@CurrentUser SysUser optuser ) {
		sysGlobalCustomService.update(sysGlobalCustom,optuser);
		return SUCC;
	}
	/**
	 * 按主键删除
	 * 
	 * @param ids
	 *@return
	 */
	@RequestMapping("deleteAll")
	@ResponseBody
	public Tip deleteAll(String ids, @CurrentUser SysUser optuser) {
		if (ids == null) {
			return SUCC;
		}
		sysGlobalCustomService.deleteAll(ids, optuser);
		return SUCC;
	}
	/**
	 * 查询逻辑
	 * 
	 * @param pageNumber
	 * @param pageSize
	 * @param user
	 * @return
	 */
	@RequestMapping("query")
	@ResponseBody
	public PageQuery<SysGlobalCustom> query(@RequestParam(defaultValue = "1") Integer pageNumber, Integer pageSize,
			SysGlobalCustom sysGlobalCustom) {
		PageQuery<SysGlobalCustom> query = new PageQuery<SysGlobalCustom>();
		query.setPageNumber(pageNumber);
		if(pageSize!=null) {
			query.setPageSize(pageSize);
		}
		query.setParas(sysGlobalCustom);
		query = sysGlobalCustomService.pageQuery(query);
		return query;
	}
	@RequestMapping("mailconfig")
	@ResponseBody
	public Tip getMailConfig() {
		GlobalMail mail =sysGlobalCustomService.findByPrefix(new GlobalMail());
		return data(mail);
	}
}
