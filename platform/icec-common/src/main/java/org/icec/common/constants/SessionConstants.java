package org.icec.common.constants;

public class SessionConstants {
	public static final String SESSIONID="SESSIONID";
	public static final String SESSION_USERID="SESSION_USERID";
	public static final String KAPTCHA_SESSION_KEY="KAPTCHA_SESSION_KEY";
}
