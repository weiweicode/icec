package org.icec.common.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author kenny
 * @date 2019/5/5
 */
public class PhoneUtils {

    private static final String REGEX = "^1[3-9](\\d){9}$";

    public static boolean isValidPhone(String phone) {
        if (StringUtils.isBlank(phone)) {
            return false;
        }
        // 去除空格和横杠
        phone = phone.replace(" ", "");
        phone = phone.replace("-", "");
        Pattern p = Pattern.compile(REGEX);
        Matcher m = p.matcher(phone);
        return m.matches();
    }
}
