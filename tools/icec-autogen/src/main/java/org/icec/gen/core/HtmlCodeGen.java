package org.icec.gen.core;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;

import org.beetl.core.Template;
import org.beetl.sql.core.JavaType;
import org.beetl.sql.core.UnderlinedNameConversion;
import org.beetl.sql.core.db.ColDesc;
import org.beetl.sql.core.db.TableDesc;
import org.beetl.sql.core.kit.StringKit;
import org.icec.gen.core.model.Attribute;
import org.icec.gen.core.model.Entity;
import org.springframework.util.StringUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class HtmlCodeGen implements CodeGen {
	String CR = System.getProperty("line.separator");
	String basepkg;
	String module = "";

	public HtmlCodeGen() {
		super();
	}

	public HtmlCodeGen(String basepkg) {
		super();
		this.basepkg = basepkg;
		String[] pkgArr = basepkg.split("\\.");
		module = pkgArr[pkgArr.length - 1];
	}

	public static String listTemplate = "";
	public static String addTemplate = "";
	public static String editTemplate = "";
	static {
		listTemplate = GenConfig.getTemplate("/org/icec/gen/core/htmlList.btl");
		addTemplate = GenConfig.getTemplate("/org/icec/gen/core/htmlAdd.btl");
		editTemplate = GenConfig.getTemplate("/org/icec/gen/core/htmlEdit.btl");
	}

	@Override
	public void genCode(String project, String entityPkg, String entityClass, TableDesc tableDesc, GenConfig config,
			boolean isDisplay) {
		String path = "/templates";
		String srcPath = AutoGen.getPath(project, "/src/main/resources");
		Template template = SourceGen.gt.getTemplate(listTemplate);
		String entityClassName = StringKit.toLowerCaseFirstOne(entityClass);
		Entity entity = getEntityInfo(tableDesc);
		entity.setComment(tableDesc.getRemark());
		entity.setName(entityClass);
		entity.setCode(entityClassName);
		template.binding("entity", entity);
		template.binding("module", module);
		
		Template addtemplate = SourceGen.gt.getTemplate(addTemplate);
		addtemplate.binding("entity", entity);
		addtemplate.binding("module", module);
		Template edittemplate = SourceGen.gt.getTemplate(editTemplate);
		edittemplate.binding("entity", entity);
		edittemplate.binding("module", module);
		File sqlfile = new File(srcPath + path + "/" + entityClassName);
		if (!sqlfile.exists()) {
			sqlfile.mkdirs();
		}

		try {
			
			String listhtml = template.render();
			String listTarget = srcPath + path + "/" + entityClassName + "/" + "list.html";
			File listfile = new File(listTarget);
			FileWriter writer = new FileWriter(listfile);
			writer.write(listhtml);
			writer.flush();
			writer.close();
			String addhtml = addtemplate.render();
			String addTarget = srcPath + path + "/" + entityClassName + "/" + "add.html";
			File addfile = new File(addTarget);
			FileWriter addwriter = new FileWriter(addfile);
			addwriter.write(addhtml);
			addwriter.flush();
			addwriter.close();
			String edithtml = edittemplate.render();
			String editTarget = srcPath + path + "/" + entityClassName + "/" + "edit.html";
			File editfile = new File(editTarget);
			FileWriter editwriter = new FileWriter(editfile);
			editwriter.write(edithtml);
			editwriter.flush();
			editwriter.close();
			
		} catch (IOException e) {
			throw new RuntimeException("html代码生成失败", e);
		}

		log.info("html succeed！");

	}

	public Entity getEntityInfo(TableDesc tableDesc) {
		if (tableDesc == null) {
			return null;
		}
		Entity e = new Entity();
		Set<String> cols = tableDesc.getCols();
		ArrayList<Attribute> attrs = new ArrayList<Attribute>();
		for (String col : cols) {
			ColDesc desc = tableDesc.getColDesc(col);
			Attribute attr = new Attribute();
			attr.setColName(col);
			attr.setName(new UnderlinedNameConversion().getPropertyName(col));
			if (tableDesc.getIdNames().contains(col)) {
				attr.setId(true);
				e.setIdAttribute(attr);
			}
			attr.setComment(desc.remark);
			String type = JavaType.getType(desc.sqlType, desc.size, desc.digit);
			if (type.equals("Double")) {
				type = "BigDecimal";
			}
			if (type.equals("Timestamp")) {
				type = "Date";
			}
			attr.setJavaType(type);
			setGetDisplayName(attr);
			attrs.add(attr);
		}
		e.setList(attrs);
		return e;
	}

	/* 根据数据库注释来判断显示名称 */
	private void setGetDisplayName(Attribute attr) {
		String comment = attr.getComment();
		if (StringUtils.isEmpty(comment)) {
			attr.setDisplayName(attr.getName());
			return;
		}
		String displayName = null;
		int index = comment.indexOf(",");
		if (index != -1) {
			displayName = comment.substring(0, index);
			attr.setDisplayName(displayName);
		} else {
			attr.setDisplayName(comment);
		}
	}

}
