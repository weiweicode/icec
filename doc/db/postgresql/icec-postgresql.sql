/*
Navicat PGSQL Data Transfer
Date: 2018-12-17 14:36:02
*/

CREATE SEQUENCE "public"."sys_area_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 9
 CACHE 1
 OWNED BY "public"."sys_area"."id";
 
 CREATE SEQUENCE "public"."sys_file_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 46
 CACHE 1
 OWNED BY "public"."sys_file"."id";
 CREATE SEQUENCE "public"."sys_log_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 3752
 CACHE 1
 OWNED BY "public"."sys_log"."id";
 
 CREATE SEQUENCE "public"."sys_menu_id_seq"
 INCREMENT 1
 MINVALUE 1
 MAXVALUE 9223372036854775807
 START 66
 CACHE 1
 OWNED BY "public"."sys_menu"."id";
 
 
-- ----------------------------
-- Table structure for sys_area
-- ----------------------------
DROP TABLE IF EXISTS "public"."sys_area";
CREATE TABLE "public"."sys_area" (
"id" int4 DEFAULT nextval('sys_area_id_seq'::regclass) NOT NULL,
"parent_id" int4,
"parent_ids" varchar(255) COLLATE "default",
"name" varchar(255) COLLATE "default",
"sort" int4,
"code" varchar(255) COLLATE "default",
"type" varchar(255) COLLATE "default",
"create_by" int4,
"create_date" timestamp(6),
"update_by" int4,
"update_date" timestamp(6),
"remarks" varchar(255) COLLATE "default",
"del_flag" varchar(255) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of sys_area
-- ----------------------------
INSERT INTO "public"."sys_area" VALUES ('1', '2', '0,2,', '1', '30', '11', null, '1', '2018-07-18 18:27:09.715', '1', '2018-07-18 18:27:16.474', '', '1');
INSERT INTO "public"."sys_area" VALUES ('2', '0', '0,', '合肥市', '30', '00011', '3', '1', '2018-01-12 16:08:55', '1', '2018-01-12 16:08:55', '', '0');
INSERT INTO "public"."sys_area" VALUES ('3', '2', '0,2,', '蜀山区', '30', '000111', '4', '1', '2018-01-12 16:09:12', '1', '2018-01-12 16:09:12', '', '0');
INSERT INTO "public"."sys_area" VALUES ('4', '2', '0,2,', '包河区', '30', '000112', '4', '1', '2018-01-15 10:37:16', '1', '2018-01-15 10:37:16', '', '0');

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS "public"."sys_dict";
CREATE TABLE "public"."sys_dict" (
"id" int4 NOT NULL,
"value" varchar(255) COLLATE "default",
"label" varchar(255) COLLATE "default",
"type" varchar(255) COLLATE "default",
"description" varchar(255) COLLATE "default",
"sort" int4,
"parent_id" int4,
"create_by" int4,
"create_date" timestamp(0),
"update_by" int4,
"update_date" timestamp(0),
"remarks" varchar(255) COLLATE "default",
"del_flag" varchar(255) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO "public"."sys_dict" VALUES ('-12', '4', '区县', null, null, '80', '-8', '1', '2017-12-19 00:00:00', '1', '2017-12-19 00:00:00', null, '0');
INSERT INTO "public"."sys_dict" VALUES ('-11', '3', '地市', null, null, '70', '-8', '1', '2017-12-19 00:00:00', '1', '2017-12-19 00:00:00', null, '0');
INSERT INTO "public"."sys_dict" VALUES ('-10', '2', '省份、直辖市', null, null, '60', '-8', '1', '2017-12-19 00:00:00', '1', '2017-12-19 00:00:00', null, '0');
INSERT INTO "public"."sys_dict" VALUES ('-9', '1', '国家', null, null, '50', '-8', '1', '2017-12-19 00:00:00', '1', '2017-12-19 00:00:00', null, '0');
INSERT INTO "public"."sys_dict" VALUES ('-8', null, null, 'areaType', '区域类型', '50', '0', '1', '2017-12-19 00:00:00', '1', '2017-12-19 00:00:00', null, '0');
INSERT INTO "public"."sys_dict" VALUES ('-7', '3', '三级', null, null, '50', '-4', '1', '2017-12-15 00:00:00', '1', '2017-12-15 00:00:00', null, '0');
INSERT INTO "public"."sys_dict" VALUES ('-6', '2', '二级', null, null, '50', '-4', '1', '2017-12-15 00:00:00', '1', '2017-12-15 00:00:00', null, '0');
INSERT INTO "public"."sys_dict" VALUES ('-5', '1', '一级', null, null, '50', '-4', '1', '2017-12-15 00:00:00', '1', '2017-12-15 00:00:00', null, '0');
INSERT INTO "public"."sys_dict" VALUES ('-4', null, null, 'orgLevel', '公司级别', '50', '0', '1', '2017-12-15 00:00:00', '1', '2018-07-18 16:40:35', '', '0');
INSERT INTO "public"."sys_dict" VALUES ('-3', '2', '部门', null, null, '50', '-1', '1', '2017-12-15 00:00:00', '1', '2017-12-15 00:00:00', null, '0');
INSERT INTO "public"."sys_dict" VALUES ('-2', '1', '公司', null, null, '50', '-1', '1', '2017-12-15 00:00:00', '1', '2017-12-15 00:00:00', null, '0');
INSERT INTO "public"."sys_dict" VALUES ('-1', null, null, 'orgType', '机构类型', '50', '0', '1', '2017-12-15 00:00:00', '1', '2018-07-18 17:06:42', '', '0');

-- ----------------------------
-- Table structure for sys_file
-- ----------------------------
DROP TABLE IF EXISTS "public"."sys_file";
CREATE TABLE "public"."sys_file" (
"id" int4 DEFAULT nextval('sys_file_id_seq'::regclass) NOT NULL,
"file_name" varchar(64) COLLATE "default",
"file_size" int4,
"file_type" varchar(64) COLLATE "default",
"file_url" varchar(512) COLLATE "default",
"create_time" timestamp(6),
"create_by" numeric(64),
"update_time" timestamp(6),
"update_by" numeric(64),
"busi_type" varchar(64) COLLATE "default",
"busi_no" varchar(128) COLLATE "default",
"memo" varchar(512) COLLATE "default",
"state" int4,
"deleted" int4
)
WITH (OIDS=FALSE)
;


-- ----------------------------
-- Records of sys_file
-- ----------------------------

-- ----------------------------
-- Table structure for sys_global_info
-- ----------------------------
DROP TABLE IF EXISTS "public"."sys_global_info";
CREATE TABLE "public"."sys_global_info" (
"id" int4 NOT NULL,
"type" varchar(255) COLLATE "default",
"code" varchar(255) COLLATE "default",
"value" varchar(255) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of sys_global_info
-- ----------------------------

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS "public"."sys_log";
CREATE TABLE "public"."sys_log" (
"id" int4 DEFAULT nextval('sys_log_id_seq'::regclass) NOT NULL,
"type" varchar(255) COLLATE "default",
"title" varchar(255) COLLATE "default",
"create_by" varchar(64) COLLATE "default",
"create_date" timestamp(0),
"remote_addr" varchar(255) COLLATE "default",
"user_agent" varchar(255) COLLATE "default",
"request_uri" varchar(255) COLLATE "default",
"method" varchar(255) COLLATE "default",
"lose_time" int4,
"params" text COLLATE "default",
"exception" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of sys_log
-- ----------------------------
INSERT INTO "public"."sys_log" VALUES ('1', '1', null, '', '2018-05-23 11:22:28', '0:0:0:0:0:0:0:1', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36', '/sys/login', 'GET', '4', null, null);


-- ----------------------------
-- Table structure for sys_mdict
-- ----------------------------
DROP TABLE IF EXISTS "public"."sys_mdict";
CREATE TABLE "public"."sys_mdict" (
"id" int4 NOT NULL,
"parent_id" int4,
"parent_ids" varchar(255) COLLATE "default",
"name" varchar(255) COLLATE "default",
"sort" int4,
"description" varchar(255) COLLATE "default",
"create_by" int4,
"create_date" timestamp(0),
"update_by" int4,
"update_date" timestamp(0),
"remarks" varchar(255) COLLATE "default",
"del_flag" varchar(255) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of sys_mdict
-- ----------------------------

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS "public"."sys_menu";
CREATE TABLE "public"."sys_menu" (
"id" int4 DEFAULT nextval('sys_menu_id_seq'::regclass) NOT NULL,
"parent_id" int4,
"parent_ids" varchar(255) COLLATE "default",
"name" varchar(255) COLLATE "default",
"sort" int4,
"href" varchar(255) COLLATE "default",
"target" varchar(20) COLLATE "default",
"icon" varchar(255) COLLATE "default",
"is_show" varchar(1) COLLATE "default",
"permission" varchar(255) COLLATE "default",
"create_by" int4,
"create_date" timestamp(0),
"update_by" int4,
"update_date" timestamp(0),
"remarks" varchar(255) COLLATE "default",
"del_flag" varchar(255) COLLATE "default" NOT NULL,
"type" varchar(255) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO "public"."sys_menu" VALUES ('1', '0', '0,', '系统管理', '50', '', '', 'ace-icon fa fa-cog', '1', '', '1', '2017-11-29 11:02:04', '1', '2018-04-08 15:04:24', '', '0', '1');
INSERT INTO "public"."sys_menu" VALUES ('2', '1', '0,1,', '用户管理', '30', '/sys/user/list', '', '', '1', '', '1', '2017-11-29 11:03:21', '1', '2017-11-29 11:03:21', '', '0', '1');
INSERT INTO "public"."sys_menu" VALUES ('3', '1', '0,1,', '角色管理', '40', '/sys/role/list', '', '', '1', '', '1', '2017-11-29 11:03:37', '1', '2017-11-29 11:03:37', '', '0', '1');
INSERT INTO "public"."sys_menu" VALUES ('4', '1', '0,1,', '菜单管理', '50', '/sys/menu/list', '', '', '1', '', '1', '2017-11-29 11:03:49', '1', '2017-11-29 11:03:49', '', '0', '1');
INSERT INTO "public"."sys_menu" VALUES ('5', '1', '0,1,', '机构管理', '60', '/sys/office/list', '', '', '1', '', '1', '2017-11-29 11:04:03', '1', '2017-11-29 11:04:03', '', '0', '1');
INSERT INTO "public"."sys_menu" VALUES ('6', '1', '0,1,', '区域管理', '70', '/sys/area/list', '', '', '1', '', '1', '2017-11-29 11:04:16', '1', '2017-11-29 11:04:16', '', '0', '1');
INSERT INTO "public"."sys_menu" VALUES ('7', '1', '0,1,', '字典管理', '80', '/sys/dict/list', '', '', '1', '', '1', '2017-11-29 11:04:29', '1', '2017-11-29 11:04:29', '', '0', '1');
INSERT INTO "public"."sys_menu" VALUES ('9', '1', '0,1,', '日志管理', '90', '/sys/log/list', '', '', '1', '', '1', '2017-12-01 13:53:02', '1', '2017-12-01 13:53:02', '', '0', '1');
INSERT INTO "public"."sys_menu" VALUES ('10', '4', '0,1,4', '编辑权限', '30', '', '', '', '1', 'menu:edit', '1', '2017-12-01 13:53:02', '1', '2017-12-05 14:52:34', '', '0', '2');
INSERT INTO "public"."sys_menu" VALUES ('11', '3', '0,1,3,', '编辑', '30', '', '', '', '1', 'role:edit', '1', '2017-12-05 14:54:57', '1', '2017-12-05 16:45:00', '', '0', '2');
INSERT INTO "public"."sys_menu" VALUES ('12', '2', '0,1,2,', '用户编辑', '30', '', '', '', '1', 'user:edit', '1', '2017-12-05 14:55:22', '1', '2017-12-05 14:55:22', '', '0', '2');
INSERT INTO "public"."sys_menu" VALUES ('13', '3', '0,1,3,', '查看', '30', '', '', '', '1', 'role:view', '1', '2017-12-05 15:03:15', '1', '2017-12-05 15:03:15', '', '0', '2');
INSERT INTO "public"."sys_menu" VALUES ('14', '4', '0,1,4,', '查看', '30', '', '', '', '1', 'menu:view', '1', '2017-12-05 15:03:35', '1', '2017-12-05 15:03:35', '', '0', '2');
INSERT INTO "public"."sys_menu" VALUES ('15', '3', '0,1,3,', '角色授权', '30', null, null, null, '1', 'role:auth', '1', '2017-12-01 13:53:02', '0', '2017-12-01 13:53:02', null, '0', '2');

-- ----------------------------
-- Table structure for sys_office
-- ----------------------------
DROP TABLE IF EXISTS "public"."sys_office";
CREATE TABLE "public"."sys_office" (
"id" int4 NOT NULL,
"parent_id" int4,
"parent_ids" varchar(255) COLLATE "default",
"name" varchar(255) COLLATE "default",
"sort" int4,
"area_id" int4,
"code" varchar(20) COLLATE "default",
"type" varchar(255) COLLATE "default",
"grade" varchar(255) COLLATE "default",
"address" varchar(1) COLLATE "default",
"zip_code" varchar(255) COLLATE "default",
"master" varchar(255) COLLATE "default",
"phone" varchar(255) COLLATE "default",
"fax" varchar(255) COLLATE "default",
"email" varchar(255) COLLATE "default",
"USEABLE" varchar(255) COLLATE "default",
"PRIMARY_PERSON" varchar(255) COLLATE "default",
"DEPUTY_PERSON" varchar(255) COLLATE "default",
"create_by" int4,
"create_date" timestamp(0),
"update_by" int4,
"update_date" timestamp(0),
"remarks" varchar(255) COLLATE "default",
"del_flag" varchar(255) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of sys_office
-- ----------------------------
INSERT INTO "public"."sys_office" VALUES ('1', '0', '0,', '总公司', '30', null, '001', '1', '1', null, null, null, null, null, null, '0', null, null, '1', '2017-12-06 00:00:00', '1', '2018-04-03 00:00:00', '', '0');
INSERT INTO "public"."sys_office" VALUES ('2', '1', '0,1,', '研发部', '30', '2', '002', '2', '2', null, null, null, null, null, null, '0', null, null, '1', '2018-04-03 00:00:00', '1', '2018-04-03 00:00:00', '', '0');
 
-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS "public"."sys_role";
CREATE TABLE "public"."sys_role" (
"id" int4 NOT NULL,
"office_id" varchar(255) COLLATE "default",
"name" varchar(255) COLLATE "default",
"enname" varchar(255) COLLATE "default",
"role_type" varchar(255) COLLATE "default",
"data_scope" varchar(20) COLLATE "default",
"is_sys" varchar(255) COLLATE "default",
"useable" varchar(255) COLLATE "default",
"create_by" int4,
"create_date" timestamp(0),
"update_by" int4,
"update_date" timestamp(0),
"remarks" varchar COLLATE "default",
"del_flag" varchar COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO "public"."sys_role" VALUES ('1', null, '管理员', 'admin', '1', null, '1', '1', '1', '2017-10-23 00:00:00', '1', '2018-03-19 00:00:00', '管理员', '0');
INSERT INTO "public"."sys_role" VALUES ('2', null, '普通用户', 'normal', '1', null, '1', '1', '1', '2017-11-28 00:00:00', '1', '2017-12-12 00:00:00', '', '1');
INSERT INTO "public"."sys_role" VALUES ('4', null, '普通', 'normal', '1', null, '1', '1', '1', '2017-12-14 00:00:00', '1', '2017-12-14 00:00:00', '', '0');

-- ----------------------------
-- Table structure for sys_role_area
-- ----------------------------
DROP TABLE IF EXISTS "public"."sys_role_area";
CREATE TABLE "public"."sys_role_area" (
"role_id" int4 NOT NULL,
"area_id" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of sys_role_area
-- ----------------------------
INSERT INTO "public"."sys_role_area" VALUES ('1', '2');
INSERT INTO "public"."sys_role_area" VALUES ('1', '3');
INSERT INTO "public"."sys_role_area" VALUES ('1', '4');
INSERT INTO "public"."sys_role_area" VALUES ('4', '1');
INSERT INTO "public"."sys_role_area" VALUES ('4', '2');
INSERT INTO "public"."sys_role_area" VALUES ('4', '3');
INSERT INTO "public"."sys_role_area" VALUES ('4', '4');
INSERT INTO "public"."sys_role_area" VALUES ('6', '2');
INSERT INTO "public"."sys_role_area" VALUES ('6', '3');
INSERT INTO "public"."sys_role_area" VALUES ('6', '4');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS "public"."sys_role_menu";
CREATE TABLE "public"."sys_role_menu" (
"role_id" int4 NOT NULL,
"menu_id" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO "public"."sys_role_menu" VALUES ('1', '1');
INSERT INTO "public"."sys_role_menu" VALUES ('1', '2');
INSERT INTO "public"."sys_role_menu" VALUES ('1', '3');
INSERT INTO "public"."sys_role_menu" VALUES ('1', '4');
INSERT INTO "public"."sys_role_menu" VALUES ('1', '5');
INSERT INTO "public"."sys_role_menu" VALUES ('1', '6');
INSERT INTO "public"."sys_role_menu" VALUES ('1', '7');
INSERT INTO "public"."sys_role_menu" VALUES ('1', '9');
INSERT INTO "public"."sys_role_menu" VALUES ('1', '10');
INSERT INTO "public"."sys_role_menu" VALUES ('1', '11');
INSERT INTO "public"."sys_role_menu" VALUES ('1', '12');
INSERT INTO "public"."sys_role_menu" VALUES ('1', '13');
INSERT INTO "public"."sys_role_menu" VALUES ('1', '14');
INSERT INTO "public"."sys_role_menu" VALUES ('1', '15');
INSERT INTO "public"."sys_role_menu" VALUES ('1', '16');
INSERT INTO "public"."sys_role_menu" VALUES ('1', '22');
INSERT INTO "public"."sys_role_menu" VALUES ('1', '28');
INSERT INTO "public"."sys_role_menu" VALUES ('1', '51');
INSERT INTO "public"."sys_role_menu" VALUES ('1', '55');
INSERT INTO "public"."sys_role_menu" VALUES ('1', '56');
INSERT INTO "public"."sys_role_menu" VALUES ('1', '57');
INSERT INTO "public"."sys_role_menu" VALUES ('1', '58');
INSERT INTO "public"."sys_role_menu" VALUES ('1', '59');
INSERT INTO "public"."sys_role_menu" VALUES ('1', '66');
INSERT INTO "public"."sys_role_menu" VALUES ('2', '1');
INSERT INTO "public"."sys_role_menu" VALUES ('2', '3');
INSERT INTO "public"."sys_role_menu" VALUES ('2', '4');
INSERT INTO "public"."sys_role_menu" VALUES ('2', '5');
INSERT INTO "public"."sys_role_menu" VALUES ('2', '6');
INSERT INTO "public"."sys_role_menu" VALUES ('2', '7');
INSERT INTO "public"."sys_role_menu" VALUES ('2', '9');
INSERT INTO "public"."sys_role_menu" VALUES ('2', '13');
INSERT INTO "public"."sys_role_menu" VALUES ('2', '14');
INSERT INTO "public"."sys_role_menu" VALUES ('4', '1');
INSERT INTO "public"."sys_role_menu" VALUES ('4', '2');
INSERT INTO "public"."sys_role_menu" VALUES ('4', '3');
INSERT INTO "public"."sys_role_menu" VALUES ('4', '4');
INSERT INTO "public"."sys_role_menu" VALUES ('4', '5');
INSERT INTO "public"."sys_role_menu" VALUES ('4', '6');
INSERT INTO "public"."sys_role_menu" VALUES ('4', '7');
INSERT INTO "public"."sys_role_menu" VALUES ('4', '9');
INSERT INTO "public"."sys_role_menu" VALUES ('4', '10');
INSERT INTO "public"."sys_role_menu" VALUES ('4', '11');
INSERT INTO "public"."sys_role_menu" VALUES ('4', '12');
INSERT INTO "public"."sys_role_menu" VALUES ('4', '13');
INSERT INTO "public"."sys_role_menu" VALUES ('4', '14');
INSERT INTO "public"."sys_role_menu" VALUES ('4', '15');
INSERT INTO "public"."sys_role_menu" VALUES ('5', '1');
INSERT INTO "public"."sys_role_menu" VALUES ('5', '2');
INSERT INTO "public"."sys_role_menu" VALUES ('5', '3');
INSERT INTO "public"."sys_role_menu" VALUES ('5', '4');
INSERT INTO "public"."sys_role_menu" VALUES ('5', '5');
INSERT INTO "public"."sys_role_menu" VALUES ('5', '6');
INSERT INTO "public"."sys_role_menu" VALUES ('5', '7');
INSERT INTO "public"."sys_role_menu" VALUES ('5', '9');
INSERT INTO "public"."sys_role_menu" VALUES ('5', '10');
INSERT INTO "public"."sys_role_menu" VALUES ('5', '11');
INSERT INTO "public"."sys_role_menu" VALUES ('5', '12');
INSERT INTO "public"."sys_role_menu" VALUES ('5', '13');
INSERT INTO "public"."sys_role_menu" VALUES ('5', '14');
INSERT INTO "public"."sys_role_menu" VALUES ('5', '15');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '1');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '2');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '3');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '4');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '5');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '6');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '7');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '9');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '10');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '11');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '12');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '13');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '14');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '15');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '16');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '17');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '19');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '20');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '22');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '23');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '24');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '25');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '26');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '27');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '28');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '29');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '30');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '31');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '32');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '33');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '34');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '35');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '36');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '37');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '38');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '43');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '44');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '45');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '46');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '47');
INSERT INTO "public"."sys_role_menu" VALUES ('6', '48');

-- ----------------------------
-- Table structure for sys_role_office
-- ----------------------------
DROP TABLE IF EXISTS "public"."sys_role_office";
CREATE TABLE "public"."sys_role_office" (
"role_id" int4 NOT NULL,
"office_id" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of sys_role_office
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS "public"."sys_user";
CREATE TABLE "public"."sys_user" (
"id" int4 NOT NULL,
"company_id" int4,
"office_id" int4,
"login_name" varchar(255) COLLATE "default",
"password" varchar(255) COLLATE "default",
"no" varchar(255) COLLATE "default",
"name" varchar(255) COLLATE "default",
"email" varchar(20) COLLATE "default",
"phone" varchar(255) COLLATE "default",
"mobile" varchar(255) COLLATE "default",
"user_type" varchar(255) COLLATE "default",
"photo" varchar(255) COLLATE "default",
"login_ip" varchar(255) COLLATE "default",
"login_date" timestamp(0),
"login_flag" varchar(20) COLLATE "default",
"create_by" int4,
"create_date" timestamp(0),
"update_by" int4,
"update_date" timestamp(0),
"remarks" varchar(255) COLLATE "default",
"del_flag" varchar(255) COLLATE "default" NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO "public"."sys_user" VALUES ('1', '1', '1', 'admin', '$2a$10$kFoyTiP5IGDXJW4XfOavj.LxKSYsGv.dNOyFPkVPAIfawYYGL9FX2', '1', '管理员', '', '', null, null, '46', null, null, '0', '1', '2017-10-21 00:00:00', '1', '2018-10-29 11:54:41', '', '0');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS "public"."sys_user_role";
CREATE TABLE "public"."sys_user_role" (
"user_id" int4 NOT NULL,
"role_id" int4 NOT NULL
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO "public"."sys_user_role" VALUES ('1', '1');
INSERT INTO "public"."sys_user_role" VALUES ('1', '4');
INSERT INTO "public"."sys_user_role" VALUES ('2', '1');
INSERT INTO "public"."sys_user_role" VALUES ('2', '4');
INSERT INTO "public"."sys_user_role" VALUES ('2', '5');
INSERT INTO "public"."sys_user_role" VALUES ('3', '1');
INSERT INTO "public"."sys_user_role" VALUES ('5', '4');
INSERT INTO "public"."sys_user_role" VALUES ('6', '4');
INSERT INTO "public"."sys_user_role" VALUES ('7', '6');

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table sys_area
-- ----------------------------
ALTER TABLE "public"."sys_area" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table sys_dict
-- ----------------------------
ALTER TABLE "public"."sys_dict" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table sys_file
-- ----------------------------
ALTER TABLE "public"."sys_file" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table sys_global_info
-- ----------------------------
ALTER TABLE "public"."sys_global_info" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table sys_log
-- ----------------------------
ALTER TABLE "public"."sys_log" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table sys_mdict
-- ----------------------------
ALTER TABLE "public"."sys_mdict" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table sys_menu
-- ----------------------------
ALTER TABLE "public"."sys_menu" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table sys_office
-- ----------------------------
ALTER TABLE "public"."sys_office" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table sys_role
-- ----------------------------
ALTER TABLE "public"."sys_role" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table sys_role_menu
-- ----------------------------
ALTER TABLE "public"."sys_role_menu" ADD PRIMARY KEY ("role_id", "menu_id");

-- ----------------------------
-- Primary Key structure for table sys_role_office
-- ----------------------------
ALTER TABLE "public"."sys_role_office" ADD PRIMARY KEY ("role_id", "office_id");

-- ----------------------------
-- Primary Key structure for table sys_user
-- ----------------------------
ALTER TABLE "public"."sys_user" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table sys_user_role
-- ----------------------------
ALTER TABLE "public"."sys_user_role" ADD PRIMARY KEY ("user_id", "role_id");
